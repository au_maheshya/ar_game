﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Attack::Start()
extern void Attack_Start_mB5C06732770EAD74B436151A1AFD91150C15DCD9 (void);
// 0x00000002 System.Void Attack::Update()
extern void Attack_Update_m1021FE4BBF02FC9434CD08801023326A46EE0C90 (void);
// 0x00000003 System.Void Attack::FireBall()
extern void Attack_FireBall_mD78F894CFB837D412F308DBEC52AE08B1054C837 (void);
// 0x00000004 System.Void Attack::Thunder()
extern void Attack_Thunder_m2092222A2B9DDC63E913B63F1E624D33A56F5C4D (void);
// 0x00000005 System.Void Attack::.ctor()
extern void Attack__ctor_mA801C346E8AC75760E481503AFB4DAD2D4053F6B (void);
// 0x00000006 System.Void AttackNew::Start()
extern void AttackNew_Start_m510C561CFF3869FFB65E03442B2C86C802360D63 (void);
// 0x00000007 System.Void AttackNew::Update()
extern void AttackNew_Update_mA6CCD0D82ED995FD4944DEB971427DBBB4CC95B0 (void);
// 0x00000008 System.Void AttackNew::.ctor()
extern void AttackNew__ctor_m816571F932F8C66F3E44024AD7CF528A6BFDF055 (void);
// 0x00000009 System.Void AttackUI::Start()
extern void AttackUI_Start_m569D0D7F2159DCD47974D520B6C884AF837236C0 (void);
// 0x0000000A System.Void AttackUI::Update()
extern void AttackUI_Update_m2CD9DDC79A056BA143BD40477DF80DD41AD4D152 (void);
// 0x0000000B System.Void AttackUI::FireBall()
extern void AttackUI_FireBall_m0DD03E0CC91FB126022FEE4C3FDE45F059E4B71E (void);
// 0x0000000C System.Void AttackUI::Thunder()
extern void AttackUI_Thunder_m4E9BB10BFE70E78B390BA114DEF251968EE113D0 (void);
// 0x0000000D System.Void AttackUI::DragonAttack()
extern void AttackUI_DragonAttack_m0C359A30D7C27B8139C42433E75EB57FE0F3BDE2 (void);
// 0x0000000E System.Void AttackUI::DragonDefend()
extern void AttackUI_DragonDefend_m92E4FD8EE1E032967A8B56A3B8C9160CBFDA4E63 (void);
// 0x0000000F System.Void AttackUI::DragonIdle()
extern void AttackUI_DragonIdle_mC0916597256592B41965EE072D1A9A6B6B06E11B (void);
// 0x00000010 System.Void AttackUI::SkeletonAttack()
extern void AttackUI_SkeletonAttack_mD31D467A61BDD88667AA6C05693855FD7E0C0408 (void);
// 0x00000011 System.Void AttackUI::SkeletonDefend()
extern void AttackUI_SkeletonDefend_mCE0B607BD355F70D5BAA083FCDFB014A412942DD (void);
// 0x00000012 System.Void AttackUI::.ctor()
extern void AttackUI__ctor_m9C2DF5FEFB8D714817CCECD753C7588BB7FEA66E (void);
// 0x00000013 System.Void Quit::Start()
extern void Quit_Start_m033A99F763D22B87236EB23E02243834B9CCF280 (void);
// 0x00000014 System.Void Quit::Update()
extern void Quit_Update_m0ACBFC7352DC28E4E3F0FD0CB563C8E717681F9B (void);
// 0x00000015 System.Void Quit::QuitApp()
extern void Quit_QuitApp_m6CEFD567EB7AB366AA175CBA08000DB0137E9E22 (void);
// 0x00000016 System.Void Quit::.ctor()
extern void Quit__ctor_m3F91DC1CD69CEDC58473E2984694380137C228C0 (void);
// 0x00000017 System.String VuforiaLicense::GetLicenseKey()
extern void VuforiaLicense_GetLicenseKey_m8E6E03709CDB6968425A93FB9D016DA6AFC6C91B (void);
// 0x00000018 System.Void VuforiaLicense::.ctor()
extern void VuforiaLicense__ctor_m094B92B3323DFFD79ADB696474D5D158C95E291B (void);
static Il2CppMethodPointer s_methodPointers[24] = 
{
	Attack_Start_mB5C06732770EAD74B436151A1AFD91150C15DCD9,
	Attack_Update_m1021FE4BBF02FC9434CD08801023326A46EE0C90,
	Attack_FireBall_mD78F894CFB837D412F308DBEC52AE08B1054C837,
	Attack_Thunder_m2092222A2B9DDC63E913B63F1E624D33A56F5C4D,
	Attack__ctor_mA801C346E8AC75760E481503AFB4DAD2D4053F6B,
	AttackNew_Start_m510C561CFF3869FFB65E03442B2C86C802360D63,
	AttackNew_Update_mA6CCD0D82ED995FD4944DEB971427DBBB4CC95B0,
	AttackNew__ctor_m816571F932F8C66F3E44024AD7CF528A6BFDF055,
	AttackUI_Start_m569D0D7F2159DCD47974D520B6C884AF837236C0,
	AttackUI_Update_m2CD9DDC79A056BA143BD40477DF80DD41AD4D152,
	AttackUI_FireBall_m0DD03E0CC91FB126022FEE4C3FDE45F059E4B71E,
	AttackUI_Thunder_m4E9BB10BFE70E78B390BA114DEF251968EE113D0,
	AttackUI_DragonAttack_m0C359A30D7C27B8139C42433E75EB57FE0F3BDE2,
	AttackUI_DragonDefend_m92E4FD8EE1E032967A8B56A3B8C9160CBFDA4E63,
	AttackUI_DragonIdle_mC0916597256592B41965EE072D1A9A6B6B06E11B,
	AttackUI_SkeletonAttack_mD31D467A61BDD88667AA6C05693855FD7E0C0408,
	AttackUI_SkeletonDefend_mCE0B607BD355F70D5BAA083FCDFB014A412942DD,
	AttackUI__ctor_m9C2DF5FEFB8D714817CCECD753C7588BB7FEA66E,
	Quit_Start_m033A99F763D22B87236EB23E02243834B9CCF280,
	Quit_Update_m0ACBFC7352DC28E4E3F0FD0CB563C8E717681F9B,
	Quit_QuitApp_m6CEFD567EB7AB366AA175CBA08000DB0137E9E22,
	Quit__ctor_m3F91DC1CD69CEDC58473E2984694380137C228C0,
	VuforiaLicense_GetLicenseKey_m8E6E03709CDB6968425A93FB9D016DA6AFC6C91B,
	VuforiaLicense__ctor_m094B92B3323DFFD79ADB696474D5D158C95E291B,
};
static const int32_t s_InvokerIndices[24] = 
{
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	5961,
	8945,
	5961,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	24,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
