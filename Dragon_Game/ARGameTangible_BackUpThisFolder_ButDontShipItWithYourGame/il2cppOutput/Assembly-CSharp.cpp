﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// UnityEngine.Animator
struct Animator_t8A52E42AE54F76681838FE9E632683EF3952E883;
// Attack
struct Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14;
// AttackNew
struct AttackNew_t175AD790C001A66EE4240EF7A66C152B85FB6E82;
// AttackUI
struct AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngine.ParticleSystem
struct ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1;
// Quit
struct Quit_tD52791116DF223EC8394DC3E473F3E4788F9F2F2;
// System.String
struct String_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// VuforiaLicense
struct VuforiaLicense_t558A3A93186A45EF8A9401D2262B52B5593182BC;

IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral12AB26515733BFF2215CFE9B16686C0E47F64A53;
IL2CPP_EXTERN_C String_t* _stringLiteral3515B6FC038E0865426CADE67AA7FA4C36D7A1A2;
IL2CPP_EXTERN_C String_t* _stringLiteral481F06AE46C7B698A58FCE7581749852D49AA0D0;
IL2CPP_EXTERN_C String_t* _stringLiteral9460E256DB65843C5A727E770BAF32AC7ABFBAF3;
IL2CPP_EXTERN_C String_t* _stringLiteral9B6D0CFA8AA2B1681809EF3D0539E3CCBA2D638A;
IL2CPP_EXTERN_C String_t* _stringLiteral9E120D1A7445302B601EBA5A8BC42D397DB65F6C;
IL2CPP_EXTERN_C String_t* _stringLiteralE7C4B03B801F5B1AB726438DE04A24094F6C28B7;
IL2CPP_EXTERN_C String_t* _stringLiteralFD04B72C8064DF4B7D963842C22FC87B1AC04F1C;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};
struct Il2CppArrayBounds;

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// VuforiaLicense
struct VuforiaLicense_t558A3A93186A45EF8A9401D2262B52B5593182BC  : public RuntimeObject
{
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.ParticleSystem
struct ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Animator
struct Animator_t8A52E42AE54F76681838FE9E632683EF3952E883  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// Attack
struct Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Animator Attack::animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ___animator_4;
	// UnityEngine.ParticleSystem Attack::fire
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___fire_5;
	// UnityEngine.ParticleSystem Attack::thunder
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___thunder_6;
};

// AttackNew
struct AttackNew_t175AD790C001A66EE4240EF7A66C152B85FB6E82  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject AttackNew::ball
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ball_4;
	// UnityEngine.Animator AttackNew::anim
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ___anim_5;
	// UnityEngine.ParticleSystem AttackNew::fireball
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___fireball_6;
};

// AttackUI
struct AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Animator AttackUI::animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ___animator_4;
	// UnityEngine.ParticleSystem AttackUI::fire
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___fire_5;
	// UnityEngine.ParticleSystem AttackUI::thunder
	ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* ___thunder_6;
};

// Quit
struct Quit_tD52791116DF223EC8394DC3E473F3E4788F9F2F2  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif


// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;

// T UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::Play(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9 (Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* __this, String_t* ___stateName0, const RuntimeMethod* method) ;
// System.Void Attack::FireBall()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack_FireBall_mD78F894CFB837D412F308DBEC52AE08B1054C837 (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.ParticleSystem::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_mD943E601BFE16CB9BB5D1F5E6AED5C36F5F11EF5 (ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m0D59B7EBC3A782C9FBBF107FBCD4B72B38D993B3 (int32_t ___key0, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void AttackUI::FireBall()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_FireBall_m0DD03E0CC91FB126022FEE4C3FDE45F059E4B71E (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) ;
// System.Void AttackUI::Thunder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_Thunder_m4E9BB10BFE70E78B390BA114DEF251968EE113D0 (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m965C6D4CA85A24DD95B347D22837074F19C58134 (const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Attack::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack_Start_mB5C06732770EAD74B436151A1AFD91150C15DCD9 (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator = GetComponent<Animator>();
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0;
		L_0 = Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE(__this, Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE_RuntimeMethod_var);
		__this->___animator_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___animator_4), (void*)L_0);
		// }
		return;
	}
}
// System.Void Attack::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack_Update_m1021FE4BBF02FC9434CD08801023326A46EE0C90 (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void Attack::dragonAttack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack_dragonAttack_m9416CAD21C9726A5FBDFA355137017A502DA3801 (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral481F06AE46C7B698A58FCE7581749852D49AA0D0);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.Play("idle01");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___animator_4;
		NullCheck(L_0);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_0, _stringLiteral481F06AE46C7B698A58FCE7581749852D49AA0D0, NULL);
		// FireBall();
		Attack_FireBall_mD78F894CFB837D412F308DBEC52AE08B1054C837(__this, NULL);
		// }
		return;
	}
}
// System.Void Attack::dragonDefend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack_dragonDefend_m9F6C1DEE1452ABB14103C74037DE00F9230DA10A (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12AB26515733BFF2215CFE9B16686C0E47F64A53);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.Play("defend");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___animator_4;
		NullCheck(L_0);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_0, _stringLiteral12AB26515733BFF2215CFE9B16686C0E47F64A53, NULL);
		// }
		return;
	}
}
// System.Void Attack::FireBall()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack_FireBall_mD78F894CFB837D412F308DBEC52AE08B1054C837 (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) 
{
	{
		// fire.Play();
		ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* L_0 = __this->___fire_5;
		NullCheck(L_0);
		ParticleSystem_Play_mD943E601BFE16CB9BB5D1F5E6AED5C36F5F11EF5(L_0, NULL);
		// }
		return;
	}
}
// System.Void Attack::Thunder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack_Thunder_m2092222A2B9DDC63E913B63F1E624D33A56F5C4D (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) 
{
	{
		// thunder.Play();
		ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* L_0 = __this->___thunder_6;
		NullCheck(L_0);
		ParticleSystem_Play_mD943E601BFE16CB9BB5D1F5E6AED5C36F5F11EF5(L_0, NULL);
		// }
		return;
	}
}
// System.Void Attack::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attack__ctor_mA801C346E8AC75760E481503AFB4DAD2D4053F6B (Attack_t2F0CF0A8114E4EFF1BF51894AD55F40B8849CD14* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AttackNew::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackNew_Start_m510C561CFF3869FFB65E03442B2C86C802360D63 (AttackNew_t175AD790C001A66EE4240EF7A66C152B85FB6E82* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3515B6FC038E0865426CADE67AA7FA4C36D7A1A2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Im starting......");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteral3515B6FC038E0865426CADE67AA7FA4C36D7A1A2, NULL);
		// anim = GetComponent<Animator>();
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0;
		L_0 = Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE(__this, Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE_RuntimeMethod_var);
		__this->___anim_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___anim_5), (void*)L_0);
		// }
		return;
	}
}
// System.Void AttackNew::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackNew_Update_mA6CCD0D82ED995FD4944DEB971427DBBB4CC95B0 (AttackNew_t175AD790C001A66EE4240EF7A66C152B85FB6E82* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12AB26515733BFF2215CFE9B16686C0E47F64A53);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral481F06AE46C7B698A58FCE7581749852D49AA0D0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFD04B72C8064DF4B7D963842C22FC87B1AC04F1C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Im updating......");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteralFD04B72C8064DF4B7D963842C22FC87B1AC04F1C, NULL);
		// if (Input.GetKeyDown(KeyCode.A))
		bool L_0;
		L_0 = Input_GetKeyDown_m0D59B7EBC3A782C9FBBF107FBCD4B72B38D993B3(((int32_t)97), NULL);
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		// ball.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___ball_4;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// anim.Play("idle01");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_2 = __this->___anim_5;
		NullCheck(L_2);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_2, _stringLiteral481F06AE46C7B698A58FCE7581749852D49AA0D0, NULL);
		// fireball.Play();
		ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* L_3 = __this->___fireball_6;
		NullCheck(L_3);
		ParticleSystem_Play_mD943E601BFE16CB9BB5D1F5E6AED5C36F5F11EF5(L_3, NULL);
	}

IL_003a:
	{
		// if (Input.GetKeyDown(KeyCode.S))
		bool L_4;
		L_4 = Input_GetKeyDown_m0D59B7EBC3A782C9FBBF107FBCD4B72B38D993B3(((int32_t)115), NULL);
		if (!L_4)
		{
			goto IL_005f;
		}
	}
	{
		// ball.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___ball_4;
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		// anim.Play("defend");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_6 = __this->___anim_5;
		NullCheck(L_6);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_6, _stringLiteral12AB26515733BFF2215CFE9B16686C0E47F64A53, NULL);
	}

IL_005f:
	{
		// }
		return;
	}
}
// System.Void AttackNew::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackNew__ctor_m816571F932F8C66F3E44024AD7CF528A6BFDF055 (AttackNew_t175AD790C001A66EE4240EF7A66C152B85FB6E82* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AttackUI::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_Start_m569D0D7F2159DCD47974D520B6C884AF837236C0 (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator = GetComponent<Animator>();
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0;
		L_0 = Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE(__this, Component_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m1C9FCB4BBE56BEC6BDEF6E4BA1E5DFF91614D7CE_RuntimeMethod_var);
		__this->___animator_4 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___animator_4), (void*)L_0);
		// }
		return;
	}
}
// System.Void AttackUI::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_Update_m2CD9DDC79A056BA143BD40477DF80DD41AD4D152 (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void AttackUI::FireBall()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_FireBall_m0DD03E0CC91FB126022FEE4C3FDE45F059E4B71E (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	{
		// fire.Play();
		ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* L_0 = __this->___fire_5;
		NullCheck(L_0);
		ParticleSystem_Play_mD943E601BFE16CB9BB5D1F5E6AED5C36F5F11EF5(L_0, NULL);
		// }
		return;
	}
}
// System.Void AttackUI::Thunder()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_Thunder_m4E9BB10BFE70E78B390BA114DEF251968EE113D0 (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	{
		// thunder.Play();
		ParticleSystem_tB19986EE308BD63D36FB6025EEEAFBEDB97C67C1* L_0 = __this->___thunder_6;
		NullCheck(L_0);
		ParticleSystem_Play_mD943E601BFE16CB9BB5D1F5E6AED5C36F5F11EF5(L_0, NULL);
		// }
		return;
	}
}
// System.Void AttackUI::DragonAttack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_DragonAttack_m0C359A30D7C27B8139C42433E75EB57FE0F3BDE2 (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral481F06AE46C7B698A58FCE7581749852D49AA0D0);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.Play("idle01");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___animator_4;
		NullCheck(L_0);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_0, _stringLiteral481F06AE46C7B698A58FCE7581749852D49AA0D0, NULL);
		// FireBall();
		AttackUI_FireBall_m0DD03E0CC91FB126022FEE4C3FDE45F059E4B71E(__this, NULL);
		// }
		return;
	}
}
// System.Void AttackUI::DragonDefend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_DragonDefend_m92E4FD8EE1E032967A8B56A3B8C9160CBFDA4E63 (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12AB26515733BFF2215CFE9B16686C0E47F64A53);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.Play("defend");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___animator_4;
		NullCheck(L_0);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_0, _stringLiteral12AB26515733BFF2215CFE9B16686C0E47F64A53, NULL);
		// }
		return;
	}
}
// System.Void AttackUI::DragonIdle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_DragonIdle_mC0916597256592B41965EE072D1A9A6B6B06E11B (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7C4B03B801F5B1AB726438DE04A24094F6C28B7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.Play("idle02");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___animator_4;
		NullCheck(L_0);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_0, _stringLiteralE7C4B03B801F5B1AB726438DE04A24094F6C28B7, NULL);
		// }
		return;
	}
}
// System.Void AttackUI::SkeletonAttack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_SkeletonAttack_mD31D467A61BDD88667AA6C05693855FD7E0C0408 (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E120D1A7445302B601EBA5A8BC42D397DB65F6C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.Play("Skeleton@Attack01");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___animator_4;
		NullCheck(L_0);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_0, _stringLiteral9E120D1A7445302B601EBA5A8BC42D397DB65F6C, NULL);
		// Thunder();
		AttackUI_Thunder_m4E9BB10BFE70E78B390BA114DEF251968EE113D0(__this, NULL);
		// }
		return;
	}
}
// System.Void AttackUI::SkeletonDefend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI_SkeletonDefend_mCE0B607BD355F70D5BAA083FCDFB014A412942DD (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9460E256DB65843C5A727E770BAF32AC7ABFBAF3);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.Play("Skeleton@Damage01");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___animator_4;
		NullCheck(L_0);
		Animator_Play_m0F6A9F84B2E256E644D56C34A7A9BD622CB00FF9(L_0, _stringLiteral9460E256DB65843C5A727E770BAF32AC7ABFBAF3, NULL);
		// }
		return;
	}
}
// System.Void AttackUI::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttackUI__ctor_m9C2DF5FEFB8D714817CCECD753C7588BB7FEA66E (AttackUI_t21FAFEE52D73F1A5C918E3F5D7E68E81D9EA30D9* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Quit::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Quit_Start_m033A99F763D22B87236EB23E02243834B9CCF280 (Quit_tD52791116DF223EC8394DC3E473F3E4788F9F2F2* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void Quit::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Quit_Update_m0ACBFC7352DC28E4E3F0FD0CB563C8E717681F9B (Quit_tD52791116DF223EC8394DC3E473F3E4788F9F2F2* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void Quit::QuitApp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Quit_QuitApp_m6CEFD567EB7AB366AA175CBA08000DB0137E9E22 (Quit_tD52791116DF223EC8394DC3E473F3E4788F9F2F2* __this, const RuntimeMethod* method) 
{
	{
		// Application.Quit();
		Application_Quit_m965C6D4CA85A24DD95B347D22837074F19C58134(NULL);
		// }
		return;
	}
}
// System.Void Quit::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Quit__ctor_m3F91DC1CD69CEDC58473E2984694380137C228C0 (Quit_tD52791116DF223EC8394DC3E473F3E4788F9F2F2* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String VuforiaLicense::GetLicenseKey()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VuforiaLicense_GetLicenseKey_m8E6E03709CDB6968425A93FB9D016DA6AFC6C91B (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9B6D0CFA8AA2B1681809EF3D0539E3CCBA2D638A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return license;
		return _stringLiteral9B6D0CFA8AA2B1681809EF3D0539E3CCBA2D638A;
	}
}
// System.Void VuforiaLicense::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VuforiaLicense__ctor_m094B92B3323DFFD79ADB696474D5D158C95E291B (VuforiaLicense_t558A3A93186A45EF8A9401D2262B52B5593182BC* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
