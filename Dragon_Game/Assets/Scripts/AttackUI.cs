using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackUI : MonoBehaviour
{
    Animator animator;

    public ParticleSystem fire;
    public ParticleSystem thunder;

    //public float damage = 100f;
    //public float range = 100f;
    //public Camera fpscam;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

    }
    void FireBall()
    {
        fire.Play();

    }
    void Thunder()
    {
        thunder.Play();

    }

    public void DragonAttack()
    {
        
        animator.Play("idle01");
        FireBall();

    }
    public void DragonDefend()
    {
        
        animator.Play("defend");

    }
    //public void DragonIdle()
    //{
        
    //    animator.Play("idle02");

    //}
    public void SkeletonAttack()
    {
        animator.Play("Skeleton@Attack01");
        Thunder();

    }
    public void SkeletonDefend()
    {
        animator.Play("Skeleton@Damage01");

    }
}
