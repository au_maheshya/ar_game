using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartSystem : MonoBehaviour
{
    public GameObject[] hearts;//[0][1][2]
    private int life; //3
    private bool death;

    private int count;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        life = hearts.Length;
        Debug.Log("Life count: " + life);

    }

    // Update is called once per frame
    void Update()
    {
        if (death == true)
        {
            //Debug.Log("Dead...");
            animator.Play("Skeleton@Death01_B");
        }
    }

    private void OnParticleCollision(GameObject collision)
    {
        count = count + 1;
        Debug.Log("Hit count "+ count);
        if (collision.tag == "Fire" && count ==50)
        {
            count = 0;
            Debug.Log("Damaged...");
            TakeDamage(1);
        }
    }

    public void TakeDamage(int d)
    {
        if (life >= 1)
        {
            life -= d;
            Destroy(hearts[life].gameObject);
            if (life < 1)
            {
                death = true;
            }
        }
    }
}
