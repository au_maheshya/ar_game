using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackNew : MonoBehaviour
{
    public GameObject ball;
    Animator anim;
    public ParticleSystem fireball;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Im starting......");
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Im updating......");
        if (Input.GetKeyDown(KeyCode.A))
        {
            ball.SetActive(true);
            anim.Play("idle01");
            fireball.Play();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            ball.SetActive(false);
            anim.Play("defend");
        }
    }
}
